const express = require('express')

const app = express()

app.get('/api', (req,res) => {
  res.status(200).json({message: 'everything is awesome'})
})

let animals = ["llama", "panda"]

app.get('/api/animals', (req,res) => {
  res.status(200).json(animals)
})

app.put('/api/animals/:animal', (req,res) => {
  const animal = req.params.animal
  animals.push(animal)
  res.status(200).send()
})

app.delete('/api/animals/:animal', (req, res) => {
  const toDelete = req.params.animal
  animals = [...animals.filter(animal => animal != toDelete)]
  res.status(200).send()
})

app.listen(4500, () => { console.log("okay, we're listening") })

import './App.css';
import { useState, useEffect } from 'react'

function App() {
  const [animals, setAnimals] = useState([])

  const loadAnimals = async () => {
    const result = await fetch('/api/animals')
    const animals = await result.json()
    setAnimals(animals)
  }
  useEffect(() => {
    loadAnimals()
  }, [])

  const [newAnimal, setNewAnimal] = useState("")

  const saveNewAnimal = () => {
    (async () => {
      await fetch(`/api/animals/${newAnimal}`, { method: 'PUT' })
      await loadAnimals()
    })()
  }

  const deleteAnimal = (animal) => {
    (async () => {
      await fetch(`/api/animals/${animal}`, { method: 'DELETE' })
      await loadAnimals()
    })()
  }

  return (
    <>
      <ul>
        {animals.map(animal => (
          <li key={animal}>
            {animal}
            <button onClick={() => deleteAnimal(animal)}>Delete</button>
          </li>
        ))}
      </ul>
      <input type='text' value={newAnimal} onChange={(e) => setNewAnimal(e.target.value)}/>
      <button onClick={saveNewAnimal}>Add</button>
    </>
  );
}

export default App;

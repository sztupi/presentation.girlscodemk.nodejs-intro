const EventEmitter = require('events')

const someEmitter = new EventEmitter();

const syncHandler = (e) => {
  console.log(`some-event sync callback: ${e.value}`)
}

const asyncHandler = (e) => {
  setImmediate(() => {
    console.log(`some-event async callback: ${e.value}`)
  })
}

someEmitter.on('some-event', syncHandler)
someEmitter.on('some-event', asyncHandler)

someEmitter.emit('some-event', { value: 4 })

console.log("main func")

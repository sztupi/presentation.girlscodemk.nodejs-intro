const fs = require('fs')

const filename = process.argv.length > 2 ? process.argv[2] : 'readfile.js';
const content = fs.readFileSync(filename, { encoding: 'utf-8' });

console.log(content)

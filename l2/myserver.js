const http = require('http')

module.exports = {
  start: () => {
    const server = http.createServer((req, res) => {
      if (req.url === '/llama') {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/html')
        res.end('Hello Llama')
      } else {
        res.statusCode = 404;
        res.setHeader('Content-Type', 'text/html')
        res.end('nope, go away')
      }
    })

    server.listen(8080, '0.0.0.0', () => {
      console.log("running....")
    })

    return server;
  },

  stop: (server) => {
    server.close()
  }
}

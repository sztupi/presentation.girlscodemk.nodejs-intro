const myserver = require('./myserver')
const readline = require('readline')

const s = myserver.start();

let rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

rl.on('line', () => {
  myserver.stop(s);

  rl.close();
})

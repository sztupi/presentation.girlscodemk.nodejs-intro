const express = require('express')
const router = express.Router()

let items = ["llama", "panda"]

router.get('/', (req, res) => {
  res.json(items)
})

router.post('/', (req, res) => {
  const item = req.body.item
  if (item) {
    items.push(item)
    res.status(200).send()
  } else {
    console.log('invalid request', req.body)
    res.status(400).json({error: `invalid request`})
  }
})

router.delete('/:id', (req, res) => {
  const deleted = req.params.id
  items = [...items.filter(item => item != deleted)]
  res.status(200).send()
})


module.exports = router;

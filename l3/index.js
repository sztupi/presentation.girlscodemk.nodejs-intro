const express = require('express')
const bodyparser = require('body-parser')
const items = require('./api/items')

const app = express()

app.use(bodyparser.urlencoded({extended: false}))
app.use(bodyparser.json())

app.use("/api/items", items)

app.listen(4000, () => { console.log('running on port 4000') })
